__author__ = 'o.pasichnyk'

import asyncio
from abc import ABC, abstractmethod

import settings
from utils.rabbitmq import RabbitBasic


class BasicParser(ABC):
    def __init__(self, value: str):
        self.value = value

    @abstractmethod
    def parse(self):
        pass


class UppercaseParser(BasicParser):
    def parse(self) -> str:
        return self.value.upper()


class ParserHandler(object):
    parsers_str = (
        UppercaseParser,
        #...
        #...
    )

    parsers_list = (
        #...
        #...
    )

    def run(self, value):
       for parser in parsers_str:
            value = parser.parse(value)


if __name__ == "__main__":
    print("Parsers started...")

    parser_handler = ParserHandler()

    async def callback(ch, method, properties, body): #do process async, todo: move it
        parser_handler.run(body)

        # put to new queue
        with RabbitBasic(
                username=settings.RABBIT_USER, password=settings.RABBIT_PASSWORD, queue='parser_result') as rabbit:
            rabbit.publish(post_body)

    loop = asyncio.get_event_loop()
    loop.run_until_complete(main())

    with RabbitBasic(
            username=settings.RABBIT_USER, password=settings.RABBIT_PASSWORD, queue='parser') as rabbit:
        rabbit.consume(callback)
