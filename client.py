__author__ = 'o.pasichnyk'

import requests

import settings

#some default json to copy-paste
default_json = '{["new", "close", "open", "waiting"]}'


if __name__ == "__main__":
    server_url = f"http://{settings.HOST}:{settings.PORT}"

    data = str(input('Enter data (enter to put default dict): '))

    if not data:
        data = default_json

    print(f"Putting: {data}")
    result = requests.post(url=server_url, data=data)
    print('Processing...')
    print(f'Result...{result.body}')
