#!/usr/bin/python3
__author__ = 'o.pasichnyk'

import threading
from http.server import BaseHTTPRequestHandler, HTTPServer
from socketserver import ThreadingMixIn

import pika

import settings
from justa.utils.rabbitmq import RabbitBasic


class Server(BaseHTTPRequestHandler):
    def __init__(self, *args):
        BaseHTTPRequestHandler.__init__(self, *args)

        self.rabbit_basic = RabbitBasic(
            username=settings.RABBIT_USER, password=settings.RABBIT_PASSWORD, queue='parser')

    def handle_404(self, message: str = "") -> bytes:
        self.send_response(404)
        self.end_headers()

        return bytes(message, "UTF-8")

    def handle_200(self, message: str = "") -> bytes:
        self.send_response(200)
        self.end_headers()

        return bytes(message, "UTF-8")

    def respond(self) -> bytes:
        content = self.headers.get('Content-Length')
        post_body = ""

        if content:
            content_len = int(content)
            post_body = self.rfile.read(content_len)

        if not post_body: # temporary not allowed without data
            return self.handle_404()

        with self.rabbit_basic as rabbit:
            rabbit.publish(post_body)
            return self.handle_200()

        return self.handle_404()

    def do_POST(self):
        #print("POST logs")
        #self.wfile.write(bytes("POST logs", "utf-8"))

        self.respond()


class ThreadingSimpleServer(ThreadingMixIn, HTTPServer):
    pass


if __name__ == "__main__":
    webServer = ThreadingSimpleServer((settings.HOST, settings.PORT), Server)

    print(f"Server started http://{settings.HOST}:{settings.PORT}")

    try:
        webServer.serve_forever()
    except KeyboardInterrupt:
        pass

    webServer.server_close()

    print("Server stopped.")
