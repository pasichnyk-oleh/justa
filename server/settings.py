__author__ = 'o.pasichnyk'

import os

HOST = "0.0.0.0"
PORT = 8000

#Rabbit-mq basic settings
RABBIT_USER = os.getenv('RABBIT_USER', 'user')
RABBIT_PASSWORD = os.getenv('RABBIT_PASSWORD', 'password')
