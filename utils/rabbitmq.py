__author__ = 'o.pasichnyk'

import pika


class RabbitBasic(object):
    def __init__(self, username: str, password: str, queue: str, host: str = 'rabbitmqhost', port: int = 5672):
        credentials = pika.PlainCredentials(**{'username': username, 'password': password})
        parameters = pika.ConnectionParameters(host=host, port=port, credentials=credentials)

        self.connection = pika.BlockingConnection(parameters)
        self.channel = self.connection.channel()

        self.channel.queue_declare(queue=queue)
        self.queue = queue

    def __enter__(self):
        return self

    def __exit__(self, type, value, traceback):
        self.connection.close()

    def publish(self, body: str) -> bool:
        print(f"[x] Sent mess {body}")
        self.channel.basic_publish(exchange='', routing_key=self.queue, body=body)

        return True

    def consume(self, callback) -> bool:
        print(' [*] Waiting for messages')
        self.channel.basic_consume(queue=self.queue, on_message_callback=callback, auto_ack=True)
        self.channel.start_consuming()
