There are 3 services (docker containers):
------------

-- 1) /server on http://0.0.0.0:8000

-- 2) /parser (not available through network), listen rabbit-mq

-- 3) rabbit-mq on 15672/5672 ports

** todo: combine them with docker-compose **




1) To run and build all containers:
------------

``run.sh``


2) See containsers
------------

``docker ps``


3) To run client
------------
``python3 client.py``

