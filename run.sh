#!/bin/bash

# docker network
exec docker network create myapp_net

# server building
exec cd /server
exec docker build --tag justa-server .
exec docker run -v $(pwd):/code --network myapp_net -p 8000:8000 -it justa-server

# parser building
exec cd ../parser
exec docker build --tag justa-parser .
exec docker run -v $(pwd):/code -d --network myapp_net -it justa-parser

# rabbit-mq building
docker run -it --rm --name rabbitmq -d --network myapp_net --hostname rabbitmqhost -p 5672:5672 -p 15672:15672 -e RABBITMQ_DEFAULT_USER=user -e RABBITMQ_DEFAULT_PASS=password rabbitmq:3-management
